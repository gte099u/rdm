﻿using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{               
    public class Scenario
    {
        private readonly List<IAdjustment> _adjustments;

        public ScenarioDataSet CalculateNet()
        {
            var toRet = new ScenarioDataSet(new List<ScenarioLine>());
            var adjustmentProducts = _adjustments.Select(m=>m.GetProduct());

            return adjustmentProducts.Aggregate(toRet, (current, adjustmentProduct) => current + adjustmentProduct);
        }

        public Scenario()
        {
            _adjustments = new List<IAdjustment>();
        }

        public void AddAdjustment(IAdjustment toAdd)
        {
            _adjustments.Add(toAdd);
        }
    }    
}
