﻿using System.Collections.Generic;

namespace Rdm.Core
{
    public class CriteriaElement
    {
        public readonly Dictionary<string, string> Dimensions;
        public readonly bool IsEqualTo;

        public CriteriaElement(bool isEqualTo, Dictionary<string, string> dimensions)
        {
            Dimensions = dimensions;
            IsEqualTo = isEqualTo;
        }

        public bool IsSatisfiedBy(ScenarioLine line)
        {
            if (IsEqualTo)
            {
                foreach (var dimension in Dimensions)
                {
                    if (!line.Dimensions.ContainsKey(dimension.Key))
                        return false;

                    if (line.Dimensions[dimension.Key] != dimension.Value)
                        return false;
                }
            }
            else
            {
                foreach (var dimension in Dimensions)
                {
                    if (!line.Dimensions.ContainsKey(dimension.Key))
                        return false;

                    if (line.Dimensions[dimension.Key] == dimension.Value)
                        return false;
                }
            }

            return true;
        }
    }
}
