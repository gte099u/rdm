﻿namespace Rdm.Core
{
    public interface IOffset
    {
        decimal GetOffsetAmount(ScenarioLine line);
    }
}
