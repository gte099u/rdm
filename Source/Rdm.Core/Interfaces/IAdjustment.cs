﻿using System.Collections.Generic;

namespace Rdm.Core
{
    public interface IAdjustment
    {
        IEnumerable<IAdjustment> BaseAdjustments { get; }
        int Id { get; }
        string Name { get; set; }
        ScenarioDataSet GetProduct();
    }
}
