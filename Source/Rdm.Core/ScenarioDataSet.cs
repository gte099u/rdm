﻿using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{
    public class ScenarioDataSet : List<ScenarioLine>
    {
        public ScenarioDataSet(IEnumerable<ScenarioLine> data)
        {
            AddRange(data);
        }

        public ScenarioDataSet Copy()
        {
            var newData = this.Select(scenarioLine => new ScenarioLine(scenarioLine.Dimensions)
            {
                Amount = scenarioLine.Amount
            }).ToList();

            return new ScenarioDataSet(newData);
        }

        public ScenarioDataSet ApplyAdjustment(IAdjustment adjustment)
        {
            return this + adjustment.GetProduct();
        }

        public static ScenarioDataSet operator +(ScenarioDataSet data1, ScenarioDataSet data2)
        {
            var toRet = data1.Copy();

            foreach (var item in data2)
            {
                var existing = toRet.SingleOrDefault(m => m == item);

                if (existing != null)
                    existing.Amount += item.Amount;
                else
                    toRet.Add(item.Copy());
            }

            return toRet;
        }

        public int LineCount { get { return this.Count(); } }

        private decimal? _totalSum;
        public decimal TotalSum
        {
            get
            {
                if(_totalSum == null)
                    _totalSum = this.Sum(m => m.Amount);

                return (decimal)_totalSum;
            }
        }
    }
}
