﻿namespace Rdm.Core
{
    public class ExplicitAdjustment : AdjustmentBase
    {
        private readonly ScenarioDataSet _product;

        public ExplicitAdjustment(int id, string name, ScenarioDataSet product) : base(id, name, null)
        {
            _product = product;
        }

        public override ScenarioDataSet GetProduct()
        {
            return _product;
        }
    }
}
