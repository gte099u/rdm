﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{
    public class PercentageAdjustment : AdjustmentBase
    {
        public PercentageAdjustment(int id, string name, IEnumerable<CriteriaElement> criteria, decimal amount, IEnumerable<IOffset> offsets, IEnumerable<IAdjustment> baseAdjustments)
            : base(id, name, baseAdjustments)
        {
            _criteria = criteria;
            _amount = amount;
            _offsets = offsets;
        }

        private readonly decimal _amount;
        private readonly IEnumerable<CriteriaElement> _criteria;
        private readonly IEnumerable<IOffset> _offsets;

        public override ScenarioDataSet GetProduct()
        {
            // filter the list to lines which satisfy the criteria
            var filteredList = BaseDataSet.Where(m => _criteria.All(n => n.IsSatisfiedBy(m)));

            var newData = new List<ScenarioLine>();

            foreach (var scenarioLine in filteredList)
            {
                var newLine = new ScenarioLine(scenarioLine.Dimensions)
                {
                    // apply offsets where criteria are satisfied, multiply by percent amount
                    Amount = (scenarioLine.Amount + _offsets.Sum(m => m.GetOffsetAmount(scenarioLine))) * _amount
                };

                newData.Add(newLine);
            }

            return new ScenarioDataSet(newData);
        }
    }
}
