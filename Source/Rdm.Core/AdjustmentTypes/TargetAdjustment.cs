﻿using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{
    public class TargetAdjustment : AdjustmentBase
    {
        private readonly IEnumerable<DimensionTarget> _targets;

        public TargetAdjustment(int id, string name, IEnumerable<DimensionTarget> baTargets, IEnumerable<IAdjustment> baseAdjustments)
            : base(id, name, baseAdjustments)
        {
            _targets = baTargets;
        }

        private CompositeAdjustment AsCompositeAdjustment()
        {
            var adjustments = _targets.Select(DimensionTargetAsDistributedAdjustment);
            return new CompositeAdjustment(0, Name, adjustments, BaseAdjustments);
        }

        private DistributedAdjustment DimensionTargetAsDistributedAdjustment(DimensionTarget target)
        {
            var baselineAggregate =  BaseDataSet.Where(m => m.Dimensions[target.DimensionKey] == target.DimensionValue).Sum(m => m.Amount);

            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { target.DimensionKey, target.DimensionValue } });

            //~ may want to give this adjustment a new name?
            return new DistributedAdjustment(0, Name, new[] { criteria }, target.Amount - baselineAggregate, null, BaseAdjustments);
        }

        public override ScenarioDataSet GetProduct()
        {
            return AsCompositeAdjustment().GetProduct();
        }
    }
}
