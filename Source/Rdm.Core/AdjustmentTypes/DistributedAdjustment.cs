﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{
    public class DistributedAdjustment : AdjustmentBase
    {
        public DistributedAdjustment(int id, string name, IEnumerable<CriteriaElement> criteria, decimal amount, IEnumerable<IOffset> offsets, IEnumerable<IAdjustment> baseAdjustments)
            : base(id, name, baseAdjustments)
        {
            _criteria = criteria;
            _amount = amount;
            _offsets = offsets;
        }

        private readonly decimal _amount;
        private readonly IEnumerable<CriteriaElement> _criteria;
        private readonly IEnumerable<IOffset> _offsets;

        public override ScenarioDataSet GetProduct()
        {
            // return  for each where SatisfiesCriteria * share of amount

            var filteredDataSet = FilterAndOffsetDataSet();

            var newData = new List<ScenarioLine>();

            foreach (var scenarioLine in filteredDataSet)
            {
                var newLine = new ScenarioLine(scenarioLine.Dimensions)
                {
                    // apply offsets where criteria are satisfied, multiply by percent amount
                    Amount = CalculateAmount(scenarioLine, filteredDataSet)
                };

                newData.Add(newLine);
            }

            return new ScenarioDataSet(newData);
        }



        public decimal CalculateAmount(ScenarioLine scenarioLine, ScenarioDataSet dataSet)
        {
            return (scenarioLine.Amount / dataSet.TotalSum) * _amount;
        }

        private decimal GetTotalOffSets(ScenarioLine scenarioLine)
        {
            if (_offsets == null)
                return 0;

            return _offsets.Sum(m => m.GetOffsetAmount(scenarioLine));
        }

        public ScenarioDataSet FilterAndOffsetDataSet()
        {
            return new ScenarioDataSet(BaseDataSet.Where(m => _criteria.All(n => n.IsSatisfiedBy(m))) // filter
                .Select(m => new ScenarioLine(m.Dimensions) // project into list with offsets applied
                    {
                        Amount = m.Amount + GetTotalOffSets(m)
                    }));
        }
    }
}
