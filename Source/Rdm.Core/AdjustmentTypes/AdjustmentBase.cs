﻿using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{
    public abstract class AdjustmentBase : IAdjustment
    {
        private readonly int _id;
        private readonly IEnumerable<IAdjustment> _baseAdjustments;

        public IEnumerable<IAdjustment> BaseAdjustments { get { return _baseAdjustments; } }

        public int Id { get { return _id; } }

        public string Name { get; set; }

        protected AdjustmentBase(int id, string name, IEnumerable<IAdjustment> baseAdjustments)
        {
            _id = id;
            Name = name;
            _baseAdjustments = baseAdjustments;
        }

        public abstract ScenarioDataSet GetProduct();

        protected ScenarioDataSet BaseDataSet
        {
            get
            {
                var toRet = new ScenarioDataSet(new List<ScenarioLine>());
                return _baseAdjustments.Aggregate(toRet, (current, adjustment) => current + adjustment.GetProduct());
            }
        }
    }
}
