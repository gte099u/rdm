﻿using System.Collections.Generic;
using System.Linq;

namespace Rdm.Core
{
    public class CompositeAdjustment : AdjustmentBase
    {
        private readonly IEnumerable<IAdjustment> _adjustments;

        public CompositeAdjustment(int id, string name, IEnumerable<IAdjustment> adjustments, IEnumerable<IAdjustment> basis) : base(id, name, basis)
        {
            _adjustments = adjustments;
        }

        public override ScenarioDataSet GetProduct()
        {
            var data = new List<ScenarioLine>();

            foreach (var adjustment in _adjustments)
            {
                data.AddRange(adjustment.GetProduct());
            }

            // group and sum for any lines duplicated
            var grouped = data.GroupBy(m => m.GetHashCode()).Select(m => new ScenarioLine(m.First().Dimensions)
                {
                    Amount = m.Sum(n => n.Amount)
                });

            return new ScenarioDataSet(grouped);
        }
    }
}
