﻿namespace Rdm.Core
{
    public struct DimensionTarget
    {
        public DimensionTarget(string dimensionKey, string dimensionValue, decimal amount)
        {
            DimensionKey = dimensionKey;
            DimensionValue = dimensionValue;
            Amount = amount;
        }

        public readonly string DimensionKey;
        public readonly string DimensionValue;
        public readonly decimal Amount;
    }
}
