﻿using System.Collections.Generic;

namespace Rdm.Core
{
    public class ScenarioLine
    {
        public readonly Dictionary<string, string> Dimensions;
        public decimal Amount { get; set; }

        public ScenarioLine(Dictionary<string, string> dimensions)
        {
            Dimensions = dimensions;
        }

        protected bool Equals(ScenarioLine other)
        {
            if (Dimensions.Count != other.Dimensions.Count)
                return false;

            foreach (var dimension in Dimensions)
            {
                if (!other.Dimensions.ContainsKey(dimension.Key))
                    return false;

                if (other.Dimensions[dimension.Key] != dimension.Value)
                    return false;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ScenarioLine)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Dimensions.GetHashCode();
            }
        }

        public static bool operator == (ScenarioLine left, ScenarioLine right)
        {
            return Equals(left, right);
        }

        public static bool operator != (ScenarioLine left, ScenarioLine right)
        {
            return !Equals(left, right);
        }

        public ScenarioLine Copy()
        {
            var toRet = new ScenarioLine(Dimensions) {Amount = Amount};
            return toRet;
        }

    }
}
