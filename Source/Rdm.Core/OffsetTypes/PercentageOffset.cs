﻿namespace Rdm.Core
{
    public class PercentageOffset : IOffset
    {
        public CriteriaElement Condition { get; set; }
        public decimal Amount { get; set; }

        public decimal GetOffsetAmount(ScenarioLine line)
        {
            if (Condition.IsSatisfiedBy(line))
            {
                return line.Amount * Amount;
            }

            return 0;
        }
    }
}
