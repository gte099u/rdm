﻿namespace Rdm.Core
{
    public class ExplicitOffset : IOffset
    {
        private readonly CriteriaElement _condition;
        private readonly decimal _amount;

        public ExplicitOffset(CriteriaElement condition, decimal amount)
        {
            _condition = condition;
            _amount = amount;
        }

        public decimal GetOffsetAmount(ScenarioLine line)
        {
            if (_condition.IsSatisfiedBy(line))
            {
                return _amount;
            }

            return 0;
        }
    }
}
