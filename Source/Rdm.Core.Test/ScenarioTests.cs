﻿using System;
using System.Collections.Generic;
using Rdm.Core;
using Xunit;

namespace Cbra.Domain.Test.ScenarioPlanning
{
    public class ScenarioTests
    {
        [Fact]
        public void Scenario_With_Single_PercentAdjustment_Calculates_Net()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 10000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 20000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 40000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 80000)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);

            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var offset = new ExplicitOffset(new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" }, { "ObjectClass3", "251" } }), -5000);
            var adjustment = new PercentageAdjustment(5, "Name", new[] { criteria }, (decimal)(-.05), new[] { offset }, new[] { explicitAdjustment });

            var scenario = new Scenario();
            scenario.AddAdjustment(explicitAdjustment);
            scenario.AddAdjustment(adjustment);

            var net = scenario.CalculateNet();

            Assert.Equal(9750, net.ToArray()[0].Amount);
            Assert.Equal(19000, net.ToArray()[1].Amount);
            Assert.Equal(38000, net.ToArray()[2].Amount);
            Assert.Equal(80000, net.ToArray()[3].Amount);
        }

        [Fact]
        public void Scenario_With_Two_Chaining_PercentAdjustment_Calculates_Net()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 10000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 20000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 40000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 80000)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);

            var criteria1 = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var adjustment1 = new PercentageAdjustment(99, "Name", new[] { criteria1 }, (decimal)(-.05), new IOffset[] { }, new[] { explicitAdjustment });

            var criteria2 = new CriteriaElement(true, new Dictionary<string, string> { { "ObjectClass2", "25" } });
            var adjustment2 = new PercentageAdjustment(88, "Name", new[] { criteria2 }, (decimal)(-.1), new IOffset[] { }, new IAdjustment[] { explicitAdjustment, adjustment1 });

            var scenario = new Scenario();
            scenario.AddAdjustment(explicitAdjustment); //~ 5% Autism reduction off base
            scenario.AddAdjustment(adjustment1); //~ 5% Autism reduction off base
            scenario.AddAdjustment(adjustment2); //~ 10% Contracts reduction off net of autism reduction

            var net = scenario.CalculateNet();

            Assert.Equal(8550, Math.Round(net.ToArray()[0].Amount, 0)); // autism contracts
            Assert.Equal(17100, Math.Round(net.ToArray()[1].Amount, 0)); // autism contracts
            Assert.Equal(38000, Math.Round(net.ToArray()[2].Amount, 0)); // just autism
            Assert.Equal(72000, Math.Round(net.ToArray()[3].Amount, 0)); // just contracts 
        }

        [Fact]
        public void Scenario_With_DistributedAdjustment_and_PercentAdjustment_Calculates_Net()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 10000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 20000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 40000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 80000)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);

            var criteria1 = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var adjustment1 = new DistributedAdjustment(99, "Name", new[] { criteria1 }, 10000, new IOffset[] { }, new[] { explicitAdjustment });

            var criteria2 = new CriteriaElement(true, new Dictionary<string, string> { { "ObjectClass2", "25" } });
            var adjustment2 = new PercentageAdjustment(88, "Name", new[] { criteria2 }, (decimal)(-.1), new IOffset[] { }, new IAdjustment[] { explicitAdjustment, adjustment1 });

            var scenario = new Scenario();
            scenario.AddAdjustment(explicitAdjustment);
            scenario.AddAdjustment(adjustment1); //~ 10000 Autism bonus off base
            scenario.AddAdjustment(adjustment2); //~ 10% Contracts reduction off net of autism

            var net = scenario.CalculateNet();

            Assert.Equal(10286, Math.Round(net.ToArray()[0].Amount,0)); // autism contracts
            Assert.Equal(20571, Math.Round(net.ToArray()[1].Amount,0)); // autism contracts
            Assert.Equal(45714, Math.Round(net.ToArray()[2].Amount,0)); // just autism
            Assert.Equal(72000, Math.Round(net.ToArray()[3].Amount,0)); // just contracts 
        }

        [Fact]
        public void Scenario_With_Three_Explicit_Adjustments_Calculates_Net()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 10000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 20000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 40000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 80000)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);

            var criteria1 = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var adjustment1 = new DistributedAdjustment(99, "Name", new[] { criteria1 }, 10000, new IOffset[] { }, new[] { explicitAdjustment });

            var criteria2 = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "317" } });
            var adjustment2 = new DistributedAdjustment(111, "Name", new[] { criteria2 }, 10000, new IOffset[] { }, new IAdjustment[] { explicitAdjustment, adjustment1 });

            var criteria3 = new CriteriaElement(true, new Dictionary<string, string> { { "ObjectClass2", "41" } });
            var adjustment3 = new DistributedAdjustment(222, "Name", new[] { criteria3 }, 10000, new IOffset[] { }, new IAdjustment[] { explicitAdjustment, adjustment1, adjustment2 });

            var scenario = new Scenario();
            scenario.AddAdjustment(explicitAdjustment);
            scenario.AddAdjustment(adjustment1); //~ 10000 Autism bonus off base
            scenario.AddAdjustment(adjustment2); //~ 10000 317 bonus
            scenario.AddAdjustment(adjustment3); //~ 10000 grants bonus

            var net = scenario.CalculateNet();

            Assert.Equal(11429, Math.Round(net.ToArray()[0].Amount, 0)); // autism contracts
            Assert.Equal(22857, Math.Round(net.ToArray()[1].Amount, 0)); // autism contracts
            Assert.Equal(55714, Math.Round(net.ToArray()[2].Amount, 0)); // just autism 41
            Assert.Equal(90000, Math.Round(net.ToArray()[3].Amount, 0)); // 317
        }

        [Fact]
        public void Scenario_With_TargetSpec()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "11", "251", 50000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 70000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 20000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "412", 10000),

                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "11", "251", 30000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "252", 90000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "41", "411", 80000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "41", "412", 20000)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);


            var spec = new List<DimensionTarget>
            {
                new DimensionTarget("BudgetActivity", "Autism", 200000),
                new DimensionTarget ("BudgetActivity", "317", 180000)
            };

            var targetAdjustment = new TargetAdjustment(0, "Name", spec, new[] { explicitAdjustment });

            var scenario = new Scenario();

            scenario.AddAdjustment(explicitAdjustment);
            scenario.AddAdjustment(targetAdjustment);

            var net = scenario.CalculateNet();

            Assert.Equal(66667, Math.Round(net.ToArray()[0].Amount, 0));
            Assert.Equal(93333, Math.Round(net.ToArray()[1].Amount, 0));
            Assert.Equal(24545, Math.Round(net.ToArray()[4].Amount, 0));
            Assert.Equal(73636, Math.Round(net.ToArray()[5].Amount, 0));
        }
    }
}
