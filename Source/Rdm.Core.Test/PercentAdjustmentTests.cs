﻿using System.Collections.Generic;
using System.Linq;
using Rdm.Core;
using Xunit;

namespace Cbra.Domain.Test.ScenarioPlanning
{
    public class PercentAdjustmentTests
    {
        [Fact]
        public void PercentAdjustment_GetProduct_Returns_Line_Per_Criteria_Match()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 200),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 400),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 800)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);

            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var adjustment = new PercentageAdjustment(5, "Name", new[] { criteria }, (decimal)(-.05), new IOffset[] { }, new[] { explicitAdjustment });

            var product = adjustment.GetProduct();

            Assert.Equal(3, product.LineCount);
        }

        [Fact]
        public void PercentAdjustment_GetProduct_Returns_Percent_Calculated()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 200),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 400),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 800)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);

            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var adjustment = new PercentageAdjustment(5, "Name", new[] { criteria }, (decimal)(-.05), new IOffset[] { }, new[] { explicitAdjustment });

            var product = adjustment.GetProduct();

            Assert.Equal(-5, product.Single(m => m == data[0]).Amount);
            Assert.Equal(-10, product.Single(m => m == data[1]).Amount);
            Assert.Equal(-20, product.Single(m => m == data[2]).Amount);
        }

        [Fact]
        public void PercentAdjustment_With_Offset_GetProduct_Returns_Percent_Calculated()
        {
            var data = new List<ScenarioLine>
            {
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 10000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "252", 20000),
                ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "41", "411", 40000),
                ScenarioLineTestHelpers.CreateLine("317", "NCBDDD", "25", "251", 80000)
            };

            var sds = new ScenarioDataSet(data);
            var explicitAdjustment = new ExplicitAdjustment(1, "Name", sds);            

            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });
            var offset = new ExplicitOffset(new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" }, { "ObjectClass3", "251" } }), -5000);
            var adjustment = new PercentageAdjustment(5, "Name", new[] { criteria }, (decimal)(-.05), new[] { offset }, new[] { explicitAdjustment });

            var product = adjustment.GetProduct();

            // 10000 - 5000 = 5000 * -.05 = -250
            Assert.Equal(-250, product.Single(m => m == data[0]).Amount);
            Assert.Equal(-1000, product.Single(m => m == data[1]).Amount);
            Assert.Equal(-2000, product.Single(m => m == data[2]).Amount);
        }
    }
}
