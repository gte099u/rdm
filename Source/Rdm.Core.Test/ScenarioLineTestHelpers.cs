﻿using System.Collections.Generic;
using Rdm.Core;

namespace Cbra.Domain.Test
{
    public class ScenarioLineTestHelpers
    {
        public static ScenarioLine CreateLine(string ba, string cio, string oc2, string oc3, decimal amount)
        {
            return new ScenarioLine(new Dictionary<string, string>
                {
                    {"BudgetActivity", ba},
                    {"Cio", cio},
                    {"ObjectClass2", oc2},
                    {"ObjectClass3", oc3}
                }) { Amount = amount };
        }
    }
}
