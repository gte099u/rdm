﻿using System.Collections.Generic;
using Rdm.Core;
using Xunit;

namespace Cbra.Domain.Test.ScenarioPlanning
{
    public class ScenarioLineTests
    {
        [Fact]
        public void Equals_Is_True_If_Dimensions_Match()
        {
            var line1 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 200);

            Assert.True(line1.Equals(line2));
        }

        [Fact]
        public void Equals_Is_True_With_Different_Order()
        {
            var line1 = new ScenarioLine(new Dictionary<string, string>
                {
                    {"BudgetActivity", "BA1"},
                    {"Cio", "CIO"},
                    {"ObjectClass2", "OC2"},
                    {"ObjectClass3", "OC3"}
                }) { Amount = 100 };

            var line2 = new ScenarioLine(new Dictionary<string, string>
                {
                    {"ObjectClass2", "OC2"},
                    {"Cio", "CIO"},
                    {"BudgetActivity", "BA1"},                                        
                    {"ObjectClass3", "OC3"}
                }) { Amount = 300 };

            Assert.True(line1.Equals(line2));
        }

        [Fact]
        public void Equals_Is_False_If_Dimensions_Different_Length()
        {
            var line1 = new ScenarioLine(new Dictionary<string, string>
                {
                    {"BudgetActivity", "BA1"},
                    {"Cio", "CIO"},
                    {"ObjectClass2", "OC2"},
                    {"ObjectClass3", "OC3"}
                }) { Amount = 100 };

            var line2 = new ScenarioLine(new Dictionary<string, string>
                {
                    {"BudgetActivity", "BA1"},
                    {"Cio", "CIO"},
                    {"ObjectClass2", "OC2"}
                }) { Amount = 100 };

            Assert.False(line1.Equals(line2));
        }

        [Fact]
        public void Equals_Is_False_If_Keys_Mismatch()
        {
            var line1 = new ScenarioLine(new Dictionary<string, string>
                {
                    {"BudgetActivity", "BA1"},
                    {"Cio", "CIO"},
                    {"ObjectClass2", "OC2"},
                    {"ObjectClass3", "OC3"}
                }) { Amount = 100 };

            var line2 = new ScenarioLine(new Dictionary<string, string>
                {
                    {"MismatchKey", "BA1"},
                    {"Cio", "CIO"},
                    {"ObjectClass2", "OC2"},
                    {"ObjectClass3", "OC3"}
                }) { Amount = 300 };

            Assert.False(line1.Equals(line2));
        }

        [Fact]
        public void Opperator_Equals_Returns_True_If_Lines_Equal()
        {
            var line1 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 200);

            Assert.True(line1 == line2);
        }

        [Fact]
        public void Opperator_Equals_Returns_False_If_Lines_Not_Equal()
        {
            var line1 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "253", 200);

            Assert.False(line1 == line2);
        }

        [Fact]
        public void Opperator_NotEquals_Returns_True_If_Lines_Not_Equal()
        {
            var line1 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "253", 200);

            Assert.True(line1 != line2);
        }

        [Fact]
        public void Opperator_NotEquals_Returns_False_If_Lines_Equal()
        {
            var line1 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 200);

            Assert.False(line1 != line2);
        }
    }
}
