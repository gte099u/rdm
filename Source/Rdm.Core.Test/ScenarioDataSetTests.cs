﻿using System.Collections.Generic;
using System.Linq;
using Rdm.Core;
using Xunit;

namespace Cbra.Domain.Test
{

    public class ScenarioDataSetTests
    {
        [Fact]
        public void Copy_Returns_New_DataSet_With_Same_Number_Of_DataLines()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "412", 8);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);

            var set = new ScenarioDataSet(data);

            var copy = set.Copy();

            Assert.Equal(set.LineCount, copy.LineCount);
        }

        [Fact]
        public void Copy_Returns_New_DataSet_With_Same_Total_Sum()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "412", 8);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);

            var set = new ScenarioDataSet(data);

            var copy = set.Copy();

            Assert.Equal(set.TotalSum, copy.TotalSum);
        }

        [Fact]
        public void LineCount_Returns_Number_Of_Data_Lines()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "412", 8);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);

            var set = new ScenarioDataSet(data);

            Assert.Equal(4, set.LineCount);
        }

        [Fact]
        public void TotalSum_Returns_Sum_Of_Amount()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "412", 8);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);

            var set = new ScenarioDataSet(data);

            Assert.Equal(17, set.TotalSum);
        }

        [Fact]
        public void GetBaAggregates_Returns_Aggregate_Per_Unique_Ba()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD",  "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD",  "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD",  "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA2", "ICE",    "41", "412", 8);
            var line5 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD",  "41", "412", 13);
            var line6 = ScenarioLineTestHelpers.CreateLine("BA3", "NCHHSTP","41", "412", 17);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);
            data.Add(line5);
            data.Add(line6);


            var set = new ScenarioDataSet(data);
           // var aggs = set.GetBaAggregates();

           // Assert.Equal(3, aggs.Count());
        }

        [Fact]
        public void GetBaAggregates_Returns_Aggregate_Sum_Per_Ba()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA2", "ICE", "41", "412", 8);
            var line5 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD", "41", "412", 13);
            var line6 = ScenarioLineTestHelpers.CreateLine("BA3", "NCHHSTP", "41", "412", 17);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);
            data.Add(line5);
            data.Add(line6);

            var set = new ScenarioDataSet(data);
           // var aggs = set.GetBaAggregates().ToList();

            //Assert.Equal(9, aggs.Single(m => m.BudgetActivity == "BA1").Total);
            //Assert.Equal(21, aggs.Single(m => m.BudgetActivity == "BA2").Total);
            //Assert.Equal(17, aggs.Single(m => m.BudgetActivity == "BA3").Total);
        }

        [Fact]
        public void GetCioAggregates_Returns_Aggregate_Per_Unique_Ba_Cio_Combo()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA2", "ICE", "41", "412", 8);
            var line5 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD", "41", "412", 13);
            var line6 = ScenarioLineTestHelpers.CreateLine("BA3", "NCHHSTP", "41", "412", 17);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);
            data.Add(line5);
            data.Add(line6);

            var set = new ScenarioDataSet(data);
            //var aggs = set.GetCioAggregates().ToList();

            //Assert.Equal(5, aggs.Count);
        }

        [Fact]
        public void GetOc2Aggregates_Returns_Aggregate_Sum_Per_Unique_Ba_Cio_Oc2_Combo()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA2", "ICE", "41", "412", 8);
            var line5 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD", "41", "412", 13);
            var line6 = ScenarioLineTestHelpers.CreateLine("BA3", "NCHHSTP", "41", "412", 17);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);
            data.Add(line5);
            data.Add(line6);

            var set = new ScenarioDataSet(data);
            //var aggs = set.GetOc2Aggregates().ToList();

            //Assert.Equal(5, aggs.Count);
        }

        [Fact]
        public void GetCioDistribution_Returns_One_Element_Per_Aggregate()
        {
            var data = new List<ScenarioLine>();


            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 200);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 500);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA2", "ICE", "41", "412", 400);
            var line5 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD", "41", "412", 1000);
            var line6 = ScenarioLineTestHelpers.CreateLine("BA3", "NCHHSTP", "41", "412", 800);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);
            data.Add(line5);
            data.Add(line6);

            var set = new ScenarioDataSet(data);
            //var dists = set.GetCioDistributions().ToList();

            //Assert.Equal(4, dists.Count);
        }

        [Fact]
        public void GetCioDistribution_Returns_Calculated_Distribution()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 100);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 200);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 700);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA2", "ICE", "41", "412", 400);
            var line5 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD", "41", "412", 1000);
            var line6 = ScenarioLineTestHelpers.CreateLine("BA3", "NCHHSTP", "41", "412", 800);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);
            data.Add(line5);
            data.Add(line6);

            var set = new ScenarioDataSet(data);
            //var dists = set.GetCioDistributions().ToList();

            // 100 + 200 = .3 of the total 1000 for BA1

            //Assert.Equal((decimal).3, dists.Single(m => m.Cio == "NCIRD" && m.BudgetActivity == "BA1").Distribution);
        }

        [Fact]
        public void Add_Does_Not_Use_Reference_To_Same_Lines()
        {
            var data = new List<ScenarioLine>();

            var line1 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "11", "111", 1);
            var line2 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "25", "251", 3);
            var line3 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "411", 5);
            var line4 = ScenarioLineTestHelpers.CreateLine("BA1", "NCIRD", "41", "412", 8);

            data.Add(line1);
            data.Add(line2);
            data.Add(line3);
            data.Add(line4);

            var set1 = new ScenarioDataSet(data);

            var data2 = new List<ScenarioLine>();
           
            var line24 = ScenarioLineTestHelpers.CreateLine("BA2", "NCIRD", "41", "412", 100);

            data2.Add(line24);

            var set2 = new ScenarioDataSet(data2);

            // assert expected value for result of addition
            var result = set1 + set2;
            Assert.Equal(100, result[4].Amount);

            // change a value in a line
            line24.Amount = 200;
            Assert.Equal(200, set2[0].Amount);

            Assert.Equal(100, result[4].Amount);
        }
    }
}
