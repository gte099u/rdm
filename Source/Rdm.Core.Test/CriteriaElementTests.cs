﻿using System.Collections.Generic;
using Rdm.Core;
using Xunit;

namespace Cbra.Domain.Test.ScenarioPlanning
{
    public class CriteriaElementTests
    {
        [Fact]
        public void IsEqual_CriteriaElement_IsSatisfiedBy_Single_Dimension()
        {
            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" } });

            var line = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);

            Assert.True(criteria.IsSatisfiedBy(line));
        }

        [Fact]
        public void IsEqual_CriteriaElement_Not_IsSatisfiedBy_Single_Dimension()
        {
            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "autism" } });

            var line = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);

            Assert.False(criteria.IsSatisfiedBy(line));
        }

        [Fact]
        public void IsEqual_CriteriaElement_IsSatisfiedBy_Multi_Dimension()
        {
            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" }, { "ObjectClass2", "25" } });

            var line = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);

            Assert.True(criteria.IsSatisfiedBy(line));
        }

        [Fact]
        public void IsEqual_CriteriaElement_Not_IsSatisfiedBy_Multi_Dimension()
        {
            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" }, { "ObjectClass2", "30" } });

            var line = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);

            Assert.False(criteria.IsSatisfiedBy(line));
        }

        [Fact]
        public void IsEqual_CriteriaElement_Not_IsSatisfiedBy_Mismatched_Dimension()
        {
            var criteria = new CriteriaElement(true, new Dictionary<string, string> { { "BudgetActivity", "Autism" }, { "Mismatch", "25" } });

            var line = ScenarioLineTestHelpers.CreateLine("Autism", "NCBDDD", "25", "251", 100);

            Assert.False(criteria.IsSatisfiedBy(line));
        }
    }
}
