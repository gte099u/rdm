# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Welcome to RDM, a resource distribution management library ###

* RDM is a lightweight library for modeling resource distribution scenarios. Given an initial baseline distribution of resources, use RDM to apply various types of "adjustments" to model the resulting impact to the overall distribution.

Adjustment types include:

* percent adjustments (increase or decrease the distributed amount by X%)
* distributed adjustments (distribute a set amount of resources proportionately across recipient lines)
* target adjustment (back-calculate the necessary adjustment to bring recipient lines to a desired target profile)
* uniform adjustment (uniformly distribute X amount of resources to all recipient lines)
* explicit adjustment (distribute explicitly prescribed amounts to recipient lines)
* composite adjustment (group multiple adjustments into one)

Adjustments may be chained, as order of operations matters.  e.g. apply an explicit increase of 100 units to some lines prior to taking a 5% reduction across all lines.
